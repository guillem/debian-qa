#!/usr/bin/perl

use strict;
use warnings;
use YAML;
use DB_File;

undef $/; # slurp mode
my $yaml = <>;
$yaml =~ s/^  - /    - /gm; # the cgi produces broken yaml
my $autoremovals = Load($yaml);

my $db_filename = 'autoremovals-new.db';
my %db;
my $db_file = tie %db, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
    or die "Can't open database $db_filename : $!";

# 'obexftp' => {
#     'version' => '0.23-1.2',
#     'rdeps' => [
#                  'obexfs'
#                ],
#     'removal_date' => '2014-04-29 20:25:53',
#     'dependencies_only' => 'false',
#     'last_checked' => '2014-04-08 06:18:12',
#     'bugs' => [
#                 '739777'
#               ],
#     'source' => 'obexftp'
#   },

foreach my $pkg (keys %$autoremovals) {
	my $data = $autoremovals->{$pkg};
	$db{$pkg} = "$data->{version} to be removed from testing on $data->{removal_date}";
}
